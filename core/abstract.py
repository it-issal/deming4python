import os, sys

class Flavor(object):
    factory = []

    @classmethod
    def register(cls, hnd):
        Flavor.factory.append(hnd)

        return hnd

    @classmethod
    def detect(cls, prj):
        for dtp in Flavor.factory:
            inst = dtp(prj)

            if inst.detect():
                inst.invoke('prepare')

                return inst

        return None

    def __init__(self, prj):
        self._prj = prj

        self.invoke('initialize')

    def invoke(self, method, *args, **kwargs):
        hnd = getattr(self, method, None)

        if callable(hnd):
            return hnd(*args, **kwargs)
        else:
            return None

    name    = property(lambda self: self.__class__.__name__)
    project = property(lambda self: self._prj)

    def activate(self):
        self.invoke('build')

        self.invoke('install')

        self.invoke('deploy')

    def deactivate(self):
        self.invoke('remove')

        self.invoke('clean')

    def rpath(self, *path):
        return os.path.join(self.project.git_path, *path)

    def exists(self, *path):
        return os.path.exists(self.rpath(*path))

    def shell(self, *cmd):
        def clean_shell(x):
            if ' ' in x:
                x = '"' + x + '"'

            return x

        os.system(' '.join([
            clean_shell(x)
            for x in cmd
        ]))

    def __str__(self):     return str(getattr(self, 'TITLE', self.name))
    def __unicode__(self): return unicode(getattr(self, 'TITLE', self.name))
