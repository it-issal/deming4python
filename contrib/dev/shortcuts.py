#-*- coding: utf-8 -*-

from code2use.shortcuts import *

#*******************************************************************************

from .models import *
from .forms  import *
from .tasks  import *

#*******************************************************************************

LANG_LIST = (
    'php', 'python', 'nodejs', 'java', 'arduino',
)

#*******************************************************************************

STACK_LIST = (
    'ronin', 'noh',
)

CHARM_LIST = (
    'deming', 'kungfu', 'beacon', 'demantia', 'sparkle', 'hive', 'shinigami'
)

#*******************************************************************************

CORES_LIST = (
    'senility', 'octopus',
)
