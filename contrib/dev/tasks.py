#-*- coding: utf-8 -*-

from .utils import *

from .models import *
from .forms  import *

################################################################################

from bitbucket.bitbucket import Bitbucket

@shared_task
def sync_bitbucket(pseudo, passwd):
    cnx = Bitbucket(pseudo, passwd)

    st, lst = cnx.repository.all()

    for entry in lst:
        item = ScmRepository(
            provider = 'bitbucket',
            owner    = entry['owner'],
            slug     = entry['slug'],
        )

        del entry['owner']
        del entry['slug']

        for k,v in dict(
            title='title', summary='description', website='website',
            #branches='branches', issues='issues',
            #commits='changesets', events='events',
            #followers='followers',
            scm_type='scm', language='language',
            is_private='is_private', has_wiki='has_wiki',
            #fork_of='is_fork', has_forks='has_forks',
            curr_size='size', created_at='created_on', updated_at='last_updated',
        ).iteritems():
            if v in entry:
                item[k] = entry[v]

                del entry[v]

        item['raw']    = entry

        item.save()

        #print "\t -> Crawled repo : %s" % item

#*******************************************************************************
