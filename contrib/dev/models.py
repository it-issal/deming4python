#-*- coding: utf-8 -*-

from .utils import *

################################################################################

import datetime

class ScmRepository(mongo.Document):
    provider   = mongo.StringField(max_length=64, required=True)
    owner      = mongo.StringField(max_length=64, required=True)
    slug       = mongo.StringField(max_length=128, required=True)

    title      = mongo.StringField(max_length=256, required=False)
    summary    = mongo.TextField()
    website    = mongo.StringField(max_length=512, required=False)
    logo       = mongo.StringField(max_length=512, required=False)

    branches   = mongo.DictField()
    commits    = mongo.DictField()
    issues     = mongo.DictField()
    events     = mongo.DictField()
    followers  = mongo.DictField()

    scm_type   = mongo.StringField(default='git', choices=['git', 'svn', 'hg', 'bzr'])
    language   = mongo.StringField(max_length=512, required=False)

    is_private = mongo.BooleanField(default=True)
    has_wiki   = mongo.BooleanField(default=False)

    fork_of    = mongo.DictField(required=False)
    has_forks  = mongo.DictField(required=False)

    curr_size  = mongo.IntField(default=0)
    created_at = mongo.DateTimeField(default=datetime.datetime.now)
    updated_at = mongo.DateTimeField(default=datetime.datetime.now)

    raw        = mongo.DictField()
