#-*- coding: utf-8 -*-

from deming.contrib.dev.shortcuts import *

################################################################################

@fqdn.route(r'^$', strategy='login')
def homepage(context):
    context.template = 'dev/views/homepage.html'

    return {'version': version}

################################################################################

from . import basis
from . import metalogy

################################################################################

urlpatterns = fqdn.urlpatters
