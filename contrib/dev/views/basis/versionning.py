#-*- coding: utf-8 -*-

from deming.contrib.dev.shortcuts import *

################################################################################

@fqdn.route(r'^repos/sync$', strategy='login')
def synchronize(context):
    for dutty in [
        sync_bitbucket,
    ]:
        dutty('shivha', 'NicraXoft0')

    return context.redirect('/repos/')

#*******************************************************************************

@fqdn.route(r'^repos/?$', strategy='login')
def listing(context):
    context.template = 'dev/rest/repo/list.html'

    lst = []
    
    for key,hnd in ('github',): # , 'bitbucket')
        if key in context.social:
            for repo in context.social[key].repos:
                lst += [repo]

    return {
        'listing': ScmRepository.objects(),
    }

#*******************************************************************************

@fqdn.route(r'^repos/(?P<provider>.+)/(?P<owner>.+)/(?P<slug>.+)$', strategy='login')
def overview(context, provider, owner, slug):
    context.template = 'dev/rest/repo/view.html'

    return {'version': version}
