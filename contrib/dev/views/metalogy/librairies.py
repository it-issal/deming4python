#-*- coding: utf-8 -*-

from deming.contrib.dev.shortcuts import *

################################################################################

@fqdn.route(r'^libraires$', strategy='login')
def listing(context):
    context.template = 'dev/rest/repo/list.html'

    return {
        'listing': ScmRepository.objects(owner='lang2use', slug__contains='4'),
    }
