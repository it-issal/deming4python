from django.contrib import admin

from deming.contrib.console.models import *

################################################################################

class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('name', 'title', 'created_on')
    list_filter  = ('created_on',)

admin.site.register(Organization, OrganizationAdmin)

#*******************************************************************************

class TeamAdmin(admin.ModelAdmin):
    list_display = ('organization', 'name', 'title', 'created_on')
    list_filter  = ('organization', 'created_on')

admin.site.register(Team, TeamAdmin)

#*******************************************************************************

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('organization', 'name', 'title', 'created_on')
    list_filter  = ('organization', 'created_on', 'teams__name')

admin.site.register(Project, ProjectAdmin)
