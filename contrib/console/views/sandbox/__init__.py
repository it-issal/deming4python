#-*- coding: utf-8 -*-

from deming.contrib.console.shortcuts import *

################################################################################

@fqdn.route(r'^sandbox$', strategy='login')
def homepage(context):
    context.template = 'console/views/sandbox/homepage.html'

    return {'version': version}

################################################################################

@fqdn.route(r'^cheatsheet$', strategy='login')
def cheatsheet(context):
    context.template = 'console/views/sandbox/cheatsheet.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^debug$', strategy='login')
def debugger(context):
    context.template = 'console/views/sandbox/debugger.html'

    return {'version': version}
