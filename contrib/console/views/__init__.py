#-*- coding: utf-8 -*-

from deming.contrib.console.shortcuts import *

################################################################################

@fqdn.route(r'^$', strategy='login')
def homepage(context):
    context.template = 'console/views/homepage.html'

    return {'version': version}

################################################################################

from . import projects

from . import sandbox

from . import shells

#*******************************************************************************

urlpatterns = fqdn.urlpatters
