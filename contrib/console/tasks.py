#-*- coding: utf-8 -*-

from .utils import *

from .models import *
from .forms  import *

################################################################################

@shared_task
def add(x, y):
    return x + y


@shared_task
def mul(x, y):
    return x * y

@shared_task
def xsum(numbers):
    return sum(numbers)
