#-*- coding: utf-8 -*-

from .utils import *

################################################################################

class Organization(models.Model):
    name         = models.CharField(max_length=64)
    title        = models.CharField(max_length=256, blank=True)

    created_on   = models.DateTimeField(auto_now_add=True)

    def __str__(self):     return str(self.title or self.name)
    def __unicode__(self): return unicode(self.title or self.name)

#*******************************************************************************

class Team(models.Model):
    organization = models.ForeignKey(Organization)

    name         = models.CharField(max_length=64)
    title        = models.CharField(max_length=256, blank=True)

    created_on   = models.DateTimeField(auto_now_add=True)

    def __str__(self):     return str(self.title or self.name)
    def __unicode__(self): return unicode(self.title or self.name)

################################################################################

class Project(models.Model):
    organization = models.ForeignKey(Organization)

    name         = models.CharField(max_length=64)
    title        = models.CharField(max_length=256, blank=True)

    teams        = models.ManyToManyField(Team, blank=True)

    progress     = property(lambda self: 57)

    created_on   = models.DateTimeField(auto_now_add=True)

    def __str__(self):     return str(self.title or self.name)
    def __unicode__(self): return unicode(self.title or self.name)
