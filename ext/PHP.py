#-*- coding: utf-8 -*-

from deming.shortcuts import *

################################################################################

@Flavor.register
class PHP(Flavor):
    TITLE = "PHP (+Composer)"

    def detect(self):
        return self.exists('index.php')

    def install(self):
        if self.exists('index.php'):
            self.shell('composer', 'install')

    def deploy(self):
        self.shell('chown', 'www-data:www-data', '-R', self.rpathg('.'))
