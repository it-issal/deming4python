#-*- coding: utf-8 -*-

from deming.shortcuts import *

################################################################################

@Flavor.register
class Python(Flavor):
    TITLE = "Python"

    def detect(self):
        return self.exists('requirements.txt')

    def initialize(self):
        pass

    def prepare(self):
        pass

    def build(self):
        if not self.exists('venv'):
            self.shell('virtualenv', '--distribute', 'venv')

    def install(self):
        if self.exists('venv'):
            self.shell('venv/bin/pip', 'install', '-r', 'requirements.txt')

    def deploy(self):
        if self.exists('setup.py'):
            self.shell('venv/bin/python', 'setup.py', 'install')

        if self.exists('manage.py'):
            self.shell('venv/bin/python', 'manage.py', 'collectstatic', '-l', '--no-input')
