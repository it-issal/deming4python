#-*- coding: utf-8 -*-

from deming.shortcuts import *

################################################################################

@Flavor.register
class Ant(Flavor):
    TITLE = "Java Ant"

    def detect(self):
        return self.exists('pom.xml')

    def build(self):
        self.shell('ant', 'debug')

    def install(self):
        self.shell('ant', 'release')

    def clean(self):
        self.shell('ant', 'clean')

################################################################################

@Flavor.register
class Maven(Flavor):
    TITLE = "Java Maven"

    def detect(self):
        return self.exists('pom.xml')

    def build(self):
        self.shell('maven', 'package')

    def install(self):
        self.shell('maven', 'install')

    def clean(self):
        self.shell('maven', 'clean')
